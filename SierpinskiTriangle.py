import pygame
import sys
import random

# Window size
frame_size_x = 1500
frame_size_y = 800

# RIGHT and LEFT mouse click
LEFT = 1
RIGHT = 3

# For know when start to draw
verticesNum = 0
# To save the 3 vertices
vertices = []
# The last pixel drew
last = [0, 0]
# For stop drawing or continue drawing clicking space
stopDraw = False
# To start drawing (when verticesNum == 3)
ini = False

#Colors
white = pygame.Color(255, 255, 255)
green = pygame.Color(0, 255, 0)
red = pygame.Color(255, 0, 0)

# Checks for errors encountered
check_errors = pygame.init()
# pygame.init() example output -> (6, 0)
# second number in tuple gives number of errors
if check_errors[1] > 0:
    print(
        f'[!] Had {check_errors[1]} errors when initialising game, exiting...')
    sys.exit(-1)
else:
    print('[+] Game successfully initialised')

# Initialise game window
pygame.display.set_caption('Triangle of Serpinski')
game_window = pygame.display.set_mode((frame_size_x, frame_size_y))

# FPS (frames per second) controller
fps_controller = pygame.time.Clock()


def show_title(color, font, size):
    score_font = pygame.font.SysFont(font, size)
    score_surface = score_font.render(
        'Select the 3 vertices on the background: ', True, color)
    score_rect = score_surface.get_rect()
    score_rect.midtop = (frame_size_x/9, 15)
    game_window.blit(score_surface, score_rect)


def drawVertex():
    for vertex in vertices:
        pygame.draw.rect(game_window, red, pygame.Rect(
            vertex[0], vertex[1], 1, 1))


def findMediumPoint(vertex, lastPoint):
    absoluteDiff = [abs((vertex[0]-lastPoint[0])/2),
                    abs((vertex[1]-lastPoint[1])/2)]
    if(vertex[0] < lastPoint[0]):
        absoluteDiff[0] += vertex[0]
    else:
        absoluteDiff[0] += lastPoint[0]
    if(vertex[1] < lastPoint[1]):
        absoluteDiff[1] += vertex[1]
    else:
        absoluteDiff[1] += lastPoint[1]
    return absoluteDiff


def iniFractal():
    drawVertex()
    lastP = findMediumPoint(vertices[0], vertices[1])
    pygame.draw.rect(game_window, red, pygame.Rect(
        lastP[0], lastP[1], 1, 1))
    return lastP


show_title(white, 'times', 20)
# Main logic
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        # Whenever a key is pressed down
        elif event.type == pygame.KEYDOWN:
            # Esc -> Create event to quit the game
            if event.key == pygame.K_ESCAPE:
                pygame.event.post(pygame.event.Event(pygame.QUIT))
            # Space -> Stop and retrieve drawing execution
            if event.key == pygame.K_SPACE:
                stopDraw = not stopDraw
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # Left mouse button -> Draw a vertex
            if event.button == LEFT and verticesNum < 3:
                vertices.append(event.pos)
                pygame.draw.rect(game_window, red, pygame.Rect(
                    vertices[verticesNum][0], vertices[verticesNum][1], 1, 1))
                verticesNum += 1
                #When 3 vertices are drew the program start to draw
                if(verticesNum == 3):
                    ini = True
                    last = iniFractal()
                    pygame.display.update()

    if(ini):
        if(not stopDraw):
            index = random.randrange(9) % 3
            last = findMediumPoint(vertices[index], last)
            pygame.draw.rect(game_window, red, pygame.Rect(
                last[0], last[1], 1, 1))

    # Refresh game screen
    pygame.display.update()
    # Refresh rate
    fps_controller.tick(1000)
